#include <iostream>
#include <vector>

using namespace std;

vector<bool> komb1 = {1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0};
vector<bool> komb2 = {0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1};

bool vece_manje(int x) {
        return x > 8;
}

int usporedi(vector<int> tri, vector<bool> poz) {
        vector<bool> x;
        int crna = 0;
        int br = 0; // broj izmjena
        for (int i = 0; i < 15; ++i) {
            if (tri[i] == 8) {
                crna = i;//odredimo di je crna
            }
            x.push_back(vece_manje(tri[i]));//punimo bool vektor sa kombinacijama 0 i 1 (puna, prugasta)
        }
        if (crna != 4) { //ako crna nije na svom predvidenom mjestu
            br++;
            bool temp = x[4];
            x[4] = 0;
            x[crna] = temp;
        }
        for (int i = 0; i < 15; ++i) {
            if (x[i] && !poz[i]) {
                br++; //raste broj izmjena s obzirom koliko ima razlika sa kombinacijama
            }
        }
        return br;
}

int broj_pomaka(vector <int> trokut) {
        int b1 = usporedi(trokut, komb1);
        int b2  = usporedi(trokut, komb2);
        if(b1 < b2)
            return b1;
        return b2;
}

int main() {
    vector<int> arr = { 4, 15, 9, 8, 10, 6, 11, 3, 14, 7, 2, 1, 13, 5, 12 };
    cout <<"Minimalni broj kombinacija je: "<< broj_pomaka(arr);
}